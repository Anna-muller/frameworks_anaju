<html>
    <body>
        <h1>Listar Aluno</h1>
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Email</th>
                <th>Data de nascimento</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td> {{ $aluno->id }} </td>
                    <td> {{ $aluno->nome }} </td>
                    <td> {{ $aluno->email }}</td>
                    <td> {{ $aluno->data_nascimento }}</td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
