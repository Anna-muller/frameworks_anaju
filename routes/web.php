<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('exemplo', [App\Http\Controllers\ExemploController::class, 'index']);

Route::get('alunos', [App\Http\Controllers\AlunoController::class, 'listar']);

Route::get('alunos/{id}', [App\Http\Controllers\AlunoController::class, 'listarID']);
